tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer iteration = 0;
}
prog    : (e+=expr | e+=comparation | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
    
comparation : ^(EQUAL e1=expr e2=expr) -> isEqual(p1={$e1.st},p2={$e2.st})
            | ^(NOTEQUAL e1=expr e2=expr) -> isEqual(p1={$e1.st},p2={$e2.st})
            ;

expr    : ^(PLUS  e1=expr e2=expr)                                 -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)                                 -> substract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)                                 -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)                                 -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {globals.hasSymbol($i1.text)}?  -> assignVar(p1={$ID.text},p2={$e2.st})
        | INT  {numer++;}                                          -> int(i={$INT.text},j={numer.toString()})
        | ID {globals.hasSymbol($ID.text)}?                        -> movId(p1={$ID.text})
        | ^(IF condition=comparation e1=expr e1=expr) {iteration++;} -> conditionalStatement(condition={$condition.st},match={$e1.st},mismatch={$e2.st},iter={iteration.toString()})
    ;